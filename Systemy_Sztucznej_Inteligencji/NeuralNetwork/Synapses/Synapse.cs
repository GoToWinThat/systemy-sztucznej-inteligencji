﻿using NeuralNetwork.Neurons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetwork.Synapses
{
    public class Synapse:ISynapse
    {
        static Random rand = new Random();
        public INeuron input { get; set; }
        public INeuron output { get; set; }
        public double weight { get; set; }
        public Synapse(INeuron inputNeuron, INeuron outputNeuron)
        {
            input = inputNeuron;
            output = outputNeuron;
            weight = rand.NextDouble();
        }
        public double GetInputValue()
        {
            return input.GetInputValue();
        }
        public double GetOutputValue()
        {
            return input.GetOutputValue();
        }
        public void UpdateWeight(double delta)
        {
            weight += delta;
        }
    }
}
