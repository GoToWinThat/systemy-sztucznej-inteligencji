﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetwork.Synapses
{
    public interface ISynapse
    {
        double weight {get;set;}
        double GetInputValue();
        double GetOutputValue();
        void UpdateWeight(double delta);
    }
}
