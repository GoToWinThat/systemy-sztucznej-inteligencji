﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetwork
{
    public class Shuffler
    {
        public static void Shuffle<T>(T[] fdata, T[] sdata)
        {
            Random rand = new Random();
            for (int i = 0; i < sdata.Length; i++)
            {
                int k = rand.Next(0, fdata.Length - 1);
                Swap<T>(fdata, i, k);
                Swap<T>(sdata, i, k);
            }
        }

        private static void Swap<T>(T[] data, int a, int b)
        {
            T temp = data[a];
            data[a] = data[b];
            data[b] = temp;
        }
    }
}
