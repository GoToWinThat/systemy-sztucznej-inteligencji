﻿using NeuralNetwork.ActivationFunctions;
using NeuralNetwork.Neurons;
using NeuralNetwork.SumFunctions;
using NeuralNetwork.Synapses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetwork.NeuralLayer
{
    public class NeuralLayer : INeuralLayer
    {
        public List<INeuron> neurons { get; set; }

        public NeuralLayer(int neuronsCounter, IActivationFunction aFunc, ISumFunction sFunc)
        {
            neurons = new List<INeuron>();
            CreateLayer(neuronsCounter, aFunc, sFunc);
        }
        private void CreateLayer(int neuronsCounter, IActivationFunction aFunc, ISumFunction sFunc)
        {
            for (int i = 0; i < neuronsCounter; i++)
            {
                neurons.Add(new Neuron(aFunc, sFunc));
            }
        }
        public List<INeuron> GetNeurons()
        {
            return neurons;
        }
    }
}
