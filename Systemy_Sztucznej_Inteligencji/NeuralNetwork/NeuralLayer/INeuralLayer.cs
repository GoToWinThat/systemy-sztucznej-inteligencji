﻿using NeuralNetwork.ActivationFunctions;
using NeuralNetwork.Neurons;
using NeuralNetwork.SumFunctions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetwork.NeuralLayer
{
    public interface INeuralLayer
    {
        List<INeuron> neurons { get; set; }
        List<INeuron> GetNeurons();
    }
}

