﻿using NeuralNetwork.Synapses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetwork.SumFunctions
{
    public class SumFunction: ISumFunction
    {
        public double CalculateInput(List<ISynapse> inputs)
        {
            return inputs.Select(x => x.GetOutputValue() * x.weight).Sum();
        }
    }
}
