﻿using NeuralNetwork.Synapses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetwork.SumFunctions
{
   public interface ISumFunction
   {
        double CalculateInput(List<ISynapse> inputs);
   }
}
