﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetwork
{
    public class NeuralClassifier
    {
        private string pathColl { get; set; }
        private string pathParam { get; set; }
        IrisDatabase dB { get; set; }

        private List<double> objectToClassifiy = new List<double>();
        public NeuralClassifier(string _pathColl, string _pathParam)
        {
            this.pathColl = _pathColl;
            this.pathParam = _pathParam;
            SaveDatabase(pathColl);
            SaveObject();
        }
        private void SaveDatabase(string path)
        {
            dB = new IrisDatabase(pathColl);
        }
        private void SaveObject()
        {
            string data = File.ReadAllText(pathParam);
            List<string> tmp = data.Split(',').Select(s => s.Replace(".", ",")).ToList();
            for (int i = 0; i < tmp.Count; i++)
            {
                objectToClassifiy.Add(Convert.ToDouble(tmp[i]));
            }
        }
        public void Classifi()
        {
            NeuralNetwork.NeuralNetwork nn = new NeuralNetwork.NeuralNetwork(4);
            nn.AddLayer(new NeuralLayer.NeuralLayer(4, new ActivationFunctions.Sigmoid(), new SumFunctions.SumFunction()));
            nn.AddLayer(new NeuralLayer.NeuralLayer(4, new ActivationFunctions.Sigmoid(), new SumFunctions.SumFunction()));
            nn.AddLayer(new NeuralLayer.NeuralLayer(4, new ActivationFunctions.Sigmoid(), new SumFunctions.SumFunction()));
            nn.AddLayer(new NeuralLayer.NeuralLayer(3, new ActivationFunctions.Sigmoid(), new SumFunctions.SumFunction()));

            nn.PushExpectedValues(dB.types);
            nn.Train(dB.data, 100);
            List<double> result = nn.CalculateOutput(dB.data[2]);
            for (int i = 0; i < result.Count() ;i++)
            {
                if ((result[i] < 0.05) && (result[i] > -0.05))
                {
                    result[i] = 0;
                }
                if ((result[i] < 1.005) && (result[i] > 0.990))
                {
                    result[i] = 1;
                }
            }
            //Komentarz !!! 
            //Do poprawy zdecydowanie bo nie jestem z tego zadowolony jak cholera, sieci są fajne a ja jestem głupi jak but
        }
    }
}
