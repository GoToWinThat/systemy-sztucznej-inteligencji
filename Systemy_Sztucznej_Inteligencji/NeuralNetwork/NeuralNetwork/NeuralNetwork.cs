﻿using NeuralNetwork.ActivationFunctions;
using NeuralNetwork.NeuralLayer;
using NeuralNetwork.Neurons;
using NeuralNetwork.SumFunctions;
using NeuralNetwork.Synapses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetwork.NeuralNetwork
{
    class NeuralNetwork
    {
        private static double learningRate = 0.05;
        public List<INeuralLayer> neuralLayers { get; set; }
        List<List<double>> layersFade { get; set; }

        double[][] expectedResult { get; set; }

        public NeuralNetwork(int inputNeurons)
        {
            neuralLayers = new List<INeuralLayer>();
            layersFade = new List<List<double>>();
            INeuralLayer inputLayer = new NeuralLayer.NeuralLayer(inputNeurons,new LinearBipolarFunction(0.5), new SumFunction());
            neuralLayers.Add(inputLayer);
        }
        public void AddLayer(INeuralLayer newNeuralLayer)
        {
            for (int i = 0; i < neuralLayers.Last().GetNeurons().Count(); i++)
            {
                for (int k = 0; k < newNeuralLayer.GetNeurons().Count(); k++)
                {
                    ISynapse connection = new Synapse(neuralLayers[neuralLayers.Count()-1].neurons[i], newNeuralLayer.neurons[k]);
                    neuralLayers[neuralLayers.Count() - 1].neurons[i].AddOutputSynapse(connection);
                    newNeuralLayer.neurons[k].AddInputSynapse(connection);
                    // console.writeline($"connection is {connection.weight}");
                }
            }
            neuralLayers.Add(newNeuralLayer);
        }  
        public void PushExpectedValues(double[][] expected)
        {
            expectedResult = expected;
        }
        public void Train(double[][] trainSet, int repeats)
        {
            double error = double.MaxValue;


            while (error / 150 > 0.001)
            {
                error = 0;
                for (int i = 0; i < trainSet.GetLength(0); i++)
                {
                    var outputs = CalculateOutput(trainSet[i]);
                    InitialFadeSize();
                    UpdateAllWeight(outputs, i);

                    error += CalculateError(outputs, i, expectedResult);
                }
                Console.WriteLine("error: " + (error / 150).ToString());
            }
        }
        private void InitialFadeSize()
        {
            layersFade= new List<List<double>>();
            for (int i=0; i<neuralLayers.Count();i++)
            {
                layersFade.Add(new List<double>());
            }
        }

        private void CalculateChangeValues(List<double> outputs, int row)
        {
            for (int i = 0; i < neuralLayers.Last().neurons.Count; i++)
            {
                layersFade.Last().Add(
                    neuralLayers.Last().neurons[i].CalculateDerivative() * 0.5*(expectedResult[row][i] - outputs[i]));
            }
            for (int k = neuralLayers.Count - 2; k > 0; k--)
                for (int i = 0; i < neuralLayers[k].neurons.Count; i++)
                {
                    layersFade[k].Add(0);
                    for (int j = 0; j < neuralLayers[k + 1].GetNeurons().Count; j++)
                    {
                        layersFade[k][i] += layersFade[k + 1][j] * neuralLayers[k].neurons[i].GetOutputSynapseWeight(j);
                    }

                    layersFade[k][i] *= neuralLayers[k].neurons[i].CalculateDerivative();
                }
        }
        private void UpdateAllWeight(List<double> currentOutput, int row)
        {
            CalculateChangeValues(currentOutput, row);
            //Przechodzimy przez wszystkie warstwy oprócz pierwszej
            for (int k = neuralLayers.Count - 1; k > 0; k--)
            {
                //Nastepnie przez wszystkie neruony w tej warstwie
                for (int i = 0; i < neuralLayers[k].neurons.Count; i++)
                {
                    //Potem przez wszystkie synapsy połączone z neuronem o indeksie "i" warstwy "k"
                    //Które łączą się ze wszystkimi neuronami wrstwy "k-1", czyli poprzedniej o indeksach od 0 do "j"
                    for (int j = 0; j < neuralLayers[k - 1].neurons.Count; j++)
                    {
                        //Aktualizujemy wage synapsy połączonej z neuronem "j" wartswy "k-1" oraz "i" warstwy "k"
                        //W taki sposób że dodajemy wyliczoną delte która równa się iloczynowi
                        // 2 * learningRate *[gradient neurona "i" wartswy "k" * wyjśćie neurona "j" warstwy "k-1" ]
                        neuralLayers[k].neurons[i].inputs[j].
                            UpdateWeight(2*learningRate*layersFade[k][i] * neuralLayers[k - 1].neurons[j].GetOutputValue());
                    }
                }
            }
        }
        public List<double> CalculateOutput(double[] input)
        {
            SetInputValues(input);
            for (int i = 1; i <= neuralLayers.Count() - 1; i++)
            {
                for (int k = 0; k < neuralLayers[i].GetNeurons().Count(); k++)
                {
                    neuralLayers[i].neurons[k].CalculateInputValue();
                    neuralLayers[i].neurons[k].CalculateOutputValue();
                }
            }
            var output = new List<double>();
            for (int i = 0; i < neuralLayers.Last().GetNeurons().Count(); i++)
            {
                output.Add(neuralLayers.Last().neurons[i].GetOutputValue());
            }
            return output;
        }
        private void SetInputValues(double[] input)
        {
            for (int i = 0; i < neuralLayers[0].GetNeurons().Count(); i++)
            {
                neuralLayers[0].neurons[i].SetInputValue(input[i]);
                neuralLayers[0].neurons[i].CalculateOutputValue();
            }
        }
        private double CalculateError(List<double> outputs, int row, double[][] ExpectedResult) 
        {
            double error = 0;
            for (int i = 0; i < outputs.Count; i++)
                error += Math.Pow(outputs[i] - ExpectedResult[row][i], 2);
            return error;
        }
        private void WriteNetwork()
        {
            foreach (var lay in neuralLayers)
            {
                foreach (var neu in lay.GetNeurons())
                {
                    Console.WriteLine("Wejscia neuronu:");
                    foreach (var inp in neu.inputs)
                    {
                        Console.WriteLine(inp.weight.ToString());
                    }
                    Console.WriteLine("Wyjścia neuronu:");
                    foreach (var outp in neu.outputs)
                    {
                        Console.WriteLine(outp.weight.ToString());
                    }
                    Console.WriteLine();
                    Console.WriteLine();
                }
            }
        }
    }
}
