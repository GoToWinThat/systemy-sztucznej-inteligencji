﻿using NeuralNetwork.Synapses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetwork.Neurons
{
    public interface INeuron
    {
        List<ISynapse> inputs { get; set; }
        List<ISynapse> outputs { get; set; }
        void SetInputValue(double inputVal);
        double GetInputValue();
        double GetOutputValue();
        void AddInputSynapse(ISynapse input);
        void AddOutputSynapse(ISynapse output);
        double GetOutputSynapseWeight(int index);
        double GetInputSynapseWeight(int index);
        void CalculateInputValue();
        void CalculateOutputValue();
        double CalculateDerivative();
    }

}
