﻿using NeuralNetwork.ActivationFunctions;
using NeuralNetwork.SumFunctions;
using NeuralNetwork.Synapses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetwork.Neurons
{
    public class Neuron:INeuron
    {
        public List<ISynapse> inputs { get; set; }
        public List<ISynapse> outputs { get; set; }

        public IActivationFunction activFunc { get; set; }

        public ISumFunction sumFunc { get; set; }

        public double inputValue { get; set; }
        public double outputValue { get; set; }

        public Neuron(IActivationFunction aFunc, ISumFunction sFunc)
        {
            activFunc = aFunc;
            sumFunc = sFunc;
            outputs = new List<ISynapse>();
            inputs = new List<ISynapse>();
        }
        public double GetInputValue()
        {
            return inputValue;
        }
        public double GetOutputValue()
        {
            return outputValue;
        }
        public void AddInputSynapse(ISynapse input)
        {
            inputs.Add(input);
        }
        public void AddOutputSynapse(ISynapse output)
        {
            outputs.Add(output);
        }
        public double GetOutputSynapseWeight(int index)
        {
            return outputs[index].weight;
        }
        public double GetInputSynapseWeight(int index)
        {
            return inputs[index].weight;
        }
        public void CalculateInputValue()
        {
            inputValue = sumFunc.CalculateInput(inputs);
        }
        public void CalculateOutputValue()
        {
            outputValue = activFunc.CalculateOutput(inputValue);
        }
        public double CalculateDerivative()
        {
            return activFunc.CalculateDifferential(this.inputValue);
        }
        public void SetInputValue(double inputVal)
        {
            inputValue = inputVal;
        }
    }
}
