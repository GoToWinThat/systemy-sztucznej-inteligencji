﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetwork.ActivationFunctions
{
    public class Sigmoid : IActivationFunction
    {
        public double CalculateOutput(double input)
        {
            double beta = 1;
            return 1 / (1 + Math.Exp(-beta * input));
        }
        public double CalculateDifferential(double input)
        {
            return Math.Exp(-input) / Math.Pow((Math.Exp(-input) + 1), 2);
        }
    }
}
