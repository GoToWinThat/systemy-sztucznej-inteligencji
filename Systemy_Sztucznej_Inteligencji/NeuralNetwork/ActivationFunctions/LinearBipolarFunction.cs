﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetwork.ActivationFunctions
{
    public class LinearBipolarFunction : IActivationFunction
    {
        private double coefficient { get; set; }

        public LinearBipolarFunction(double _coefficient)
        {
            this.coefficient = _coefficient;
        }

        public double CalculateOutput(double input)
        {
            return ((1 - Math.Exp(-coefficient * input)) / (1 + Math.Exp(-coefficient * input)));
        }
        public double CalculateDifferential(double input)
        {
            return (2 * coefficient * Math.Pow(Math.E, -coefficient * input)) / (Math.Pow(1 + Math.Pow(Math.E, -coefficient * input), 2));
        }
    }
}
