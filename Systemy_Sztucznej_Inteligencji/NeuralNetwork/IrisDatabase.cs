﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetwork
{
    public class IrisDatabase
    {
        private string path { get; set; }
        public double[][] data;
        public double[][] types;

        public IrisDatabase(string _path)
        {
            this.path = _path;
            SaveDatabase(path);
        }

        private void SaveDatabase(string path)
        {
            string[] lines = File.ReadAllLines(path);
            data = new double[lines.Length][];
            types = new double[lines.Length][];
            for (int i = 0; i < lines.Length; i++)
            {
                string[] tmp = lines[i].Split(',');
                data[i] = new double[tmp.Length - 1];
                types[i] = new double[3];
                for (int j = 0; j < tmp.Length; j++)
                {
                    if (j >= 4)
                    {
                        if (tmp[j] == "Iris-setosa")
                        {
                            types[i][0] = 0;
                            types[i][1] = 0;
                            types[i][2] = 1;
                        }
                        if (tmp[j] == "Iris-versicolor")
                        {
                            types[i][0] = 0;
                            types[i][1] = 1;
                            types[i][2] = 0;
                        }
                        if (tmp[j] == "Iris-virginica")
                        {
                            types[i][0] = 1;
                            types[i][1] = 0;
                            types[i][2] = 0;
                        }
                    }
                    else
                    {
                        data[i][j] = Convert.ToDouble(tmp[j].Replace(".", ","));
                    }
                }
            }
            for (int i = 0; i < 4; i++)
            {
                SetColumn(
                    ref data,
                    Normalizator.Normalize(GetColumn(data, i), 0.0, 1.0),
                    i
                    );
            }
            Shuffler.Shuffle(data, types);
        }


        private double[] GetColumn(double[][] matrix, int index)
        {
            double[] result = new double[matrix.GetLength(0)];
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                result[i] = matrix[i][index];
            }
            return result;
        }

        private void SetColumn(ref double[][] matrix, double[] column, int index)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                matrix[i][index] = column[i];
            }
        }
    }
}
