﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FuzzyClustering
{
    static class FileSelector
    {
        private static List<string> extensions = new List<string> { ".bmp",".png",".jpg",".tiff",".gif",".exif" };
        [STAThread]
        public static bool SelectFile(ref string path)
        {
            var fileDialog = new OpenFileDialog {Multiselect = false, Title = "Choose image file"};
            using (fileDialog)
            {
                if (fileDialog.ShowDialog() == DialogResult.OK)
                {
                    path = fileDialog.FileName;
                }
            }
            return CheckExtension(path);
        }
        private static bool CheckExtension(string path)
        {
            string extension = Path.GetExtension(path);
            if(extensions.Contains(extension))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
