﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuzzyClustering
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
             string path = "";
            Console.WriteLine("Wybierz plik graficzny: ");
             if (FileSelector.SelectFile(ref path))
             {
                 Console.WriteLine("Pobrano plik graficzny");
                 Perform(path);
             }
             else
             {
                 Console.WriteLine("Zły format pliku. Akceptowalne formaty to .bmp, .png, .jpg, .tiff, .gif, .exif");
             }
            Console.ReadKey();
        }
        private static void Perform(string path)
        {
            Console.WriteLine("Wybierz ilość klas z zakresu od 1 do 10");
            string userNumber = Console.ReadLine();
            int clusters;
            if (int.TryParse(userNumber, out clusters) && clusters >= 1 && clusters <= 10)
            {
                FuzzyClustering fuzzyClustering = new FuzzyClustering();
                fuzzyClustering.PerformClustering(path, clusters);
                Console.WriteLine("Done!");
            }
            else
            {
                Console.WriteLine("Wprowadzono złe dane!");
            }
        }
    }
}
