﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace FuzzyClustering
{
    class Point
    {
        public double x { get; set; }
        public double y { get; set; }
        public Color color { get; set; }

        public Color clusterColor { get; set; }

        public Point(double x,double y, Color color)
        {
            this.x = x;
            this.y = x;
            this.color = color;
        }
    }
}
