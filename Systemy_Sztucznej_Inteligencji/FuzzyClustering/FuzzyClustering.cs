﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuzzyClustering
{
    public class FuzzyClustering
    {
        private string saveName = "";
        private double function_J_prev = double.MaxValue;
        private double function_J = 0.0;
        private double delta=0.0;
        private int errorCounter = 0;
        private int FATALERROR = 0;
        private int accuracyCounter = 0;
        private int iterationCounter = 0;
        private int clusters = 6;
        private readonly double fuzzinessFactor = 2.0;
        private double PSNR = 0.0;
        private double[][] MembershipTable;
        private double[][] BackupMT;
        private static  Random rand = new Random();
        private Bitmap NewImage;
        private Bitmap bmp;
        private List<Point> imagePoints = new List<Point>();
        private List<Centroid> centroidPoints = new List<Centroid>();
        private List<Centroid> BackupCP = new List<Centroid>();
        public void PerformClustering(string path,int clusters)
        {
            this.clusters = clusters;
            bmp = new Bitmap(path);
            InitAllPoints(bmp);
            InitAllClusters(bmp);
            InitMembershipTable();
            CalculatedMembership();
            while(true)
            {                            
                CalculateFunction();
                CalculateAccuracy();
                CalculatedCentroid();
                CalculatedMembership();
                iterationCounter++;
                if(errorCounter >= 3 || accuracyCounter >= 4 || FATALERROR>5)
                {
                    break;
                }
            }
            SetNewImage(bmp.Width,bmp.Height);
            Console.WriteLine($"Obraz został przetworzony w {iterationCounter} iteracjach. Wskaźnik jakośći to {PSNR}");
        }
        private void CalculateAccuracy()
        {
            if (function_J_prev < function_J)
            {
                centroidPoints = BackupCP;
                MembershipTable = BackupMT;
                errorCounter++;
            }
            else
            {
                BackupCP = centroidPoints;
                BackupMT = MembershipTable;
                delta = function_J_prev - function_J;
                if (errorCounter > 0)
                {
                    FATALERROR++;
                }
                errorCounter = 0;
            }
            if(delta<15000)
            {
                accuracyCounter++;
            }
            else
            {
                accuracyCounter = 0;            
            }
            function_J_prev = function_J;
            function_J = 0.0;
            Console.WriteLine($"Wartosc funkcji: {function_J_prev}");
            Console.WriteLine($"Delta: {delta}");
            Console.WriteLine($"AccuracyCounter: {accuracyCounter}");
            Console.WriteLine($"ErrorCounter: {errorCounter}");

        }
        private double CalculatedDistance(Point p, Centroid c)
        {
            return Math.Sqrt(Math.Pow(p.color.R - c.color.R, 2.0) + Math.Pow(p.color.G - c.color.G, 2.0) + Math.Pow(p.color.B - c.color.B, 2.0));
        }
        private void CalculateFunction()
        {
            for (int n = 0; n < imagePoints.Count; n++)
            {
                for (int c = 0; c < clusters; c++)
                {
                    function_J += Math.Pow(MembershipTable[n][c], fuzzinessFactor)
                        * Math.Pow(CalculatedDistance(imagePoints[n], centroidPoints[c]), 2);
                }
            }
        }
        private void CalculatedMembership()
        {
            for (int i = 0; i < MembershipTable.GetLength(0); i++)
            {             
                for (int k = 0; k < MembershipTable[i].Length; k++)
                {
                    double memberSum = 0.0;
                    for (int l = 0; l < clusters; l++)
                    {
                        memberSum += Math.Pow((CalculatedDistance(imagePoints[i], centroidPoints[k]) / CalculatedDistance(imagePoints[i], centroidPoints[l])), (2 / (fuzzinessFactor - 1)));
                    }
                    if (double.IsNaN(memberSum))
                    {
                        MembershipTable[i][k] = 0;  
                    }
                    else
                    {
                        MembershipTable[i][k] = (double)(1.0 / memberSum);
                    }
                }
            }
       }
        private void CalculatedCentroid()
        {
            for(int k=0; k< clusters;k++)
            {
                double R = 0.0;
                double G = 0.0;
                double B = 0.0;
                double memberSum = 0.0;
                for (int n = 0; n < MembershipTable.GetLength(0); n++)
                {
                    double u = Math.Pow(MembershipTable[n][k], fuzzinessFactor);
                    memberSum += u;

                    R += u * imagePoints[n].color.R;
                    G += u * imagePoints[n].color.G;
                    B += u * imagePoints[n].color.B;
                }
                centroidPoints[k].color = Color.FromArgb((byte)(R / memberSum),
                    (byte)(G / memberSum),
                    (byte)(B / memberSum));
            }
        }

        private void SetNewImage(int width,int height)
        {
            NewImage = new Bitmap(width,height, PixelFormat.Format32bppRgb);
            FindClusterColor();
            for (int x = 0; x < NewImage.Width; x++)
            {
                for (int y = 0; y < NewImage.Height; y++)
                {
                    NewImage.SetPixel(x,y, imagePoints[x* NewImage.Height + y].clusterColor);
                }
            }

            saveName = "zapis" + DateTime.Now.Minute.ToString();
            NewImage.Save($"{saveName}.jpg");
            CalculatePSNR(NewImage);
        }
        private void FindClusterColor()
        {
            for (int x = 0; x < MembershipTable.GetLength(0); x++)
            {
                double max = double.MinValue;
                int clusterIndex = 0;
                for (int y = 0; y < MembershipTable[x].Length; y++)
                {
                    if (MembershipTable[x][y]>max)
                    {
                        clusterIndex = y;
                        max = MembershipTable[x][y];
                    }
                }
                imagePoints[x].clusterColor = centroidPoints[clusterIndex].color;
            }
        }
        private void InitAllPoints(Bitmap bmp)
        {
            for (int x = 0; x < bmp.Width; x++)
            {
                for (int y = 0; y < bmp.Height; y++)
                {
                    imagePoints.Add(new Point(x, y, bmp.GetPixel(x, y)));
                }
            }
        }
        private void InitAllClusters(Bitmap bmp)
        {
            for (int c = 0; c < clusters; c++)
            {
                int randomX = rand.Next(bmp.Width);
                int randomY = rand.Next(bmp.Height);
                centroidPoints.Add(new Centroid(bmp.GetPixel(randomX, randomY)));
            }
        }
        private void InitMembershipTable()
        {
            MembershipTable = new double[imagePoints.Count][];
            for (int i = 0; i < imagePoints.Count; i++)
            {
                MembershipTable[i] = new double[clusters];
            }

            for (int x = 0; x < MembershipTable.GetLength(0); x++)
            {
                for (int y = 0; y < MembershipTable[x].Length; y++)
                {
                    MembershipTable[x][y] = 0.0001;
                }
            }
        }
        private void CalculatePSNR(Bitmap newImage)
        {
            List<Point> newImagePoints = new List<Point>();
            for (int x = 0; x < newImage.Width; x++)
            {
                for (int y = 0; y < newImage.Height; y++)
                {
                    newImagePoints.Add(new Point(x, y, newImage.GetPixel(x, y)));
                }
            }
            double MSER = 0.0;
            double MSEG = 0.0;
            double MSEB = 0.0;
            for (int x = 0; x < newImagePoints.Count; x++)
            {
                MSER += Math.Pow((imagePoints[x].color.R - newImagePoints[x].color.R), 2);
                MSEG += Math.Pow((imagePoints[x].color.G - newImagePoints[x].color.G), 2);
                MSEB += Math.Pow((imagePoints[x].color.B - newImagePoints[x].color.B), 2);
            }
            double MSE = (MSER + MSEG + MSEB) / (3* (bmp.Width * bmp.Height));
            double max = 255 * 255;          
            PSNR = 10 * Math.Log10(max/ MSE);
            using (StreamWriter outputFile = new StreamWriter($"{saveName}.txt"))
            {
                    outputFile.WriteLine(PSNR);
            }
        }
    }
}
