﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
namespace FuzzyClustering
{
    class Centroid
    {
        public Color color { get; set; }

        public Centroid( Color color)
        {
            this.color = color;
        }
    }
}
