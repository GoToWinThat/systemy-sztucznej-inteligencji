﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace Zbiory_Miękkie
{
    public class SoftSetInference
    {
        private string pathColl {get;set;}
        private string pathParam { get; set; }

        private DataCollection dataCollection = new DataCollection();

        private List<SearchingParam> searchedCollection = new List<SearchingParam>();
        public SoftSetInference(string _pathColl,string _pathParam)
        {
            this.pathColl = _pathColl;
            this.pathParam = _pathParam;
            SetCollection();
            SetSearchedObjects();
        }

        private void SetCollection()
        {
            string[] data = File.ReadAllLines(pathColl);
            dataCollection.SetParameters(data[0].Split(',').ToList());


            for (int i = 1; i < data.Length; i++)
            {
                dataCollection.AddObject(data[i].Split(',').ToList());
            }
        }

        private void SetSearchedObjects()
        {
            string[] data = File.ReadAllLines(pathParam);


            foreach(var d in data)
            {
                string[] tokens = d.Split(',');
                searchedCollection.Add(
                    new SearchingParam
                    {
                        label = tokens[0],
                        value = Convert.ToDouble(tokens[1].Replace('.',','))
                    });
            }
        }

        public Dictionary<string,double> Solve()
        {
            List<string> _parameters = dataCollection.GetParameters();
            List<Object> _objects = dataCollection.GetObjects();
            List<double> _objectResults = new List<double>(new double[_objects.Count]);
            Dictionary<string, double> result = new Dictionary<string, double>();

            for (int i=0; i<_parameters.Count;i++)
            {
                for(int k=0;k<searchedCollection.Count;k++)
                {
                    if(_parameters[i]==searchedCollection[k].label)
                    {
                        for(int p=0; p<_objects.Count;p++)
                        {
                            _objectResults[p] += _objects[p].parameters[i]* searchedCollection[k].value;
                        }
                    }
                }
            }

            double max = _objectResults.Max();

            for (int i = 0; i < _objectResults.Count; i++)
            {
                if(_objectResults[i]== max)
                {
                    result.Add(_objects[i].label, max);
                }
            }

            return result;
        }
    }

    struct SearchingParam
    {
        public string label ;

        public double value;

        public SearchingParam(string _label, double _value)
        {
            label = _label;
            value = _value;
        }
    }
}
