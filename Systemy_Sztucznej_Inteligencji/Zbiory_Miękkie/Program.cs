﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Zbiory_Miękkie
{
    class Program
    {
        static void Main(string[] args)
        {
            string pathColl1 = "dataCollection.txt";
            string pathParam1 = "searchedCollection_1_1.txt";
            SoftSetInference ssInf1 = new SoftSetInference(pathColl1, pathParam1);
            Dictionary<string, double> result1 = ssInf1.Solve();
            foreach(var r in result1)
            {
                Console.WriteLine($"{r.Key} = {r.Value}");
            }

            Console.WriteLine("-----------------------------------------");

            string pathColl2 = "dataCollection.txt";
            string pathParam2 = "searchedCollection_1_2.txt";
            SoftSetInference ssInf2 = new SoftSetInference(pathColl2, pathParam2);
            Dictionary<string, double> result2 = ssInf2.Solve();
            foreach (var r in result2)
            {
                Console.WriteLine($"{r.Key} = {r.Value}");
            }

            Console.WriteLine("-----------------------------------------");

            string pathColl3 = "dataCollection_2.txt";
            string pathParam3 = "searchedCollection_2_1.txt";
            SoftSetInference ssInf3 = new SoftSetInference(pathColl3, pathParam3);
            Dictionary<string, double> result3 = ssInf3.Solve();
            foreach (var r in result3)
            {
                Console.WriteLine($"{r.Key} = {r.Value}");
            }

            Console.WriteLine("-----------------------------------------");

            string pathColl4 = "dataCollection_2.txt";
            string pathParam4 = "searchedCollection_2_2.txt";
            SoftSetInference ssInf4 = new SoftSetInference(pathColl4, pathParam4);
            Dictionary<string, double> result4 = ssInf4.Solve();
            foreach (var r in result4)
            {
                Console.WriteLine($"{r.Key} = {r.Value}");
            }

            Console.WriteLine("-----------------------------------------");

            string pathColl5 = "dataCollection_2.txt";
            string pathParam5 = "searchedCollection_2_3.txt";
            SoftSetInference ssInf5 = new SoftSetInference(pathColl5, pathParam5);
            Dictionary<string, double> result5 = ssInf5.Solve();
            foreach (var r in result5)
            {
                Console.WriteLine($"{r.Key} = {r.Value}");
            }
            Console.ReadKey();

            //Teoria:
           /* Zbiór miękki formalnie opisujemy jako parę(F, A), gdzie F : A → P(U),
            gdzie A to zbiór parametrów który opisuje obiekty, a P(U) to przestrzeń kombinacji tych parametrów
            Jeśli A jest zbiorem parametrów opisującymi, to możemywyznaczyć podzbiór tych obiektów, używając innego zbioru parametrów
            (P) takiego, ze P ⊂ A, wtedy(F, P) wyznacza obiekty które należą do A i P.
            Tak opisany klasyfikator zbioru miękkiego można przedstawić za pomocą tabeli relacji binarnej, gdzie obiekty mają przypisane parametry.
            Gdy obiekt posiada dany parametr wtedy wartość relacji wynosi 1, jeśli nie 0.
            Wynioskowanie polega na odpowiednim wymnożeniu odpowiadających parametrów ze zbioru P przez tebele relacji binarnej i uzyskaniu jak najlepszego wskaźnika
             */
        }
    }
}
