﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicProcessing
{
    public class DataCollection
    {
        private List<Object> objects = new List<Object>();
        public void AddObject(List<string> data)
        {
            objects.Add(
                new Object
                {
                    decision = data.FirstOrDefault(),
                    parameters = data.Skip(1).ToList()
                });
        }

        public List<Object> GetObjects()
        {
            return objects;
        }

        public List<Decision> SetDecisions()
        {
            var searchedDecisions = objects.GroupBy(x => x.decision)
            .Select(g => new { Value = g.Key, Count = g.Count() })
            .OrderByDescending(x => x.Count);

            List<Decision> result = new List<Decision>();

            foreach(var val in searchedDecisions)
            {
                result.Add(
                    new Decision
                    { 
                        decision=val.Value,
                        count=val.Count
                    });
            }

            return result;
        }
    }

    public class Object
    {
        public string decision { get; set; }

        public List<string> parameters = new List<string>();
    }

    public struct Decision
    {
        public string decision;
        public int count;
    }
}
