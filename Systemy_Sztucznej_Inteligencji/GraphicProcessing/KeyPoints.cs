﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicProcessing
{
    public class KeyPoints
    {
        private Bitmap img { get; set; }


        public KeyPoints(string _path)
        {
            img = new Bitmap(_path);

        }

        public void SearchKeyPoint( int pointCount)
        {
            Bitmap maskImg = ApplyMaskandSave(img);
            Bitmap result = new Bitmap(maskImg.Width, maskImg.Height);
            List<Point> keyPointCandidates = new List<Point>();

            for (int i = 1; i < img.Width - 1; i++)
            {
                for (int k = 1; k < img.Height - 1; k++)
                {
                    keyPointCandidates.Add(
                        new Point
                        {
                            x = i,
                            y= k,
                            brightness=CalculateBrightness(img,i,k)
                        }
                        ); ;
                }
            }

            keyPointCandidates =  keyPointCandidates.OrderByDescending(k => k.brightness).ToList();
            keyPointCandidates.RemoveRange(pointCount, keyPointCandidates.Count - pointCount);

            for (int i = 0; i < result.Width; i++)
            {
                for (int k = 0; k < result.Height; k++)
                {
                    CopyPixel(ref result, i, k, maskImg, i, k);
                }
            }

            foreach(var p in keyPointCandidates)
            {
                result.SetPixel(p.x, p.y, Color.FromArgb(255, 0, 0));
            }

            result.Save("photoKeyPoints.png");
        }
        private float CalculateBrightness(Bitmap img, int x, int y)
        {
            float result = 0;

            for (int a = -1; a < 2; a++)
            {
                for (int b = -1; b < 2; b++)
                {
                    result += img.GetPixel(x + a, y + b).GetBrightness();
                }
            }

            //We are calculating pixels in 3x3 matrix around pixel(x,y)
            return result / 9;
        }

        private void CopyPixel(ref Bitmap dest, int x, int y, Bitmap source, int a, int b)
        {
            Color pxl = source.GetPixel(a,b);
            dest.SetPixel(x, y, pxl);
        }

        private Bitmap ApplyMaskandSave(Bitmap img)
        {
            Bitmap newImage = new Bitmap(img.Width, img.Height);
            double[][] filterMatrix = new double[3][];
            filterMatrix[0] = new double[] { -1, -1, -1 };
            filterMatrix[1] = new double[] { -1, 8, -1 };
            filterMatrix[2] = new double[] { -1, -1, -1 };

            for (int i = 1; i < img.Width - 1; i++)
            {
                for (int k = 1; k < img.Height - 1; k++)
                {

                    int newR = 0;
                    int newG = 0;
                    int newB = 0;
                    int X = i;
                    int Y = k;

                    for (int a = -1; a < 2; a++)
                    {
                        for (int b = -1; b < 2; b++)
                        {
                            int mask = (int)filterMatrix[a + 1][b + 1];
                            newR += img.GetPixel(X + a, Y + b).R * mask;
                            newG += img.GetPixel(X + a, Y + b).G * mask;
                            newB += img.GetPixel(X + a, Y + b).B * mask;
                        }
                    }

                    newR = CompareRGB(newR);
                    newG = CompareRGB(newG);
                    newB = CompareRGB(newB);
                    newImage.SetPixel(i, k, Color.FromArgb(newR, newG, newB));
                    newR = newG = newB = 0;

                }
            }
            return newImage;
        }
        private int CompareRGB(int val)
        {
            if (val > 255)
                return 255;
            if (val < 0)
                return 0;
            return val;
        }

        private struct Point
        {
            public int x;
            public int y;
            public float brightness;
        }
    }
}
