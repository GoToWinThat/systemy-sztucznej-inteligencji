﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicProcessing
{
    class Program
    {
        static void Main(string[] args)
        {
            KeyPoints imgMod = new KeyPoints(@"photo.jpg");

            imgMod.SearchKeyPoint(50000);

            BayesClassifier bayesClassifier = new BayesClassifier("data.txt","searchingParams.txt");

            Console.WriteLine(bayesClassifier.Solve());

            Console.ReadKey();
        }
    }
}
