﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicProcessing
{
    public class BayesClassifier
    {
        private string pathColl { get; set; }
        private string pathParam { get; set; }

        private int paramsCount { get; set; }

        private DataCollection dataCollection = new DataCollection();

        private List<KeyVal<string,int>> searchedCollection = new List<KeyVal<string, int>>();

        private List<Decision> decisionClass = new List<Decision>();
        public BayesClassifier(string _pathColl, string _pathParam)
        {
            this.pathColl = _pathColl;
            this.pathParam = _pathParam;
            SetCollection();
            SetSearchedObjects();
        }

        private void SetCollection()
        {
            string[] data = File.ReadAllLines(pathColl);



            for (int i = 0; i < data.Length; i++)
            {
                dataCollection.AddObject(data[i].Split(',').ToList());
            }
        }

        private void SetSearchedObjects()
        {
            string[] data = File.ReadAllLines(pathParam);

            foreach (var d in data)
            {
                searchedCollection.Add(
                    new KeyVal<string, int>(d,0)
                    );
            }
        }



        public string Solve()
        {

            //Kolekcja decyzji tak/nie oraz ich ilosc
            decisionClass = dataCollection.SetDecisions();

            //Lista wszystkich przypadkow decyzja + parametry
            List<Object> _objects = dataCollection.GetObjects();

            int paramsCount = _objects[0].parameters.Count;

            //Lista class-decyzji
            List < KeyVal<string, double> > classProbability = new List<KeyVal<string, double>>();

            //Musimy wyliczyć prawdopodobieństwo dla każdej klasy
            for(int i=0; i<decisionClass.Count;i++)
            {
                double probability = (decisionClass[i].count / _objects.Count);

                //Bedziemy wyliczać prawdopodobienstwo kazdego atrybutu (pogoda/temperatura/wiatr)
                for (int p=0; p<paramsCount; p++)
                {
                    //Przeszukujemy wszystkie dane (kolumny)
                    for (int k = 0; k < _objects.Count; k++)
                    {
                        //Jeżeli rozpatrujemy przypadek dla odpowiedniej klasy (decyzja się zgadzą np. tak)
                        if (_objects[k].decision == decisionClass[i].decision)
                        {
                            //Jeżeli poszukiwany obiekt posiada dany typ parametru np. Pogoda: DESZCZOWO, to inkrementujemy wystąpienie tego atrybutu
                            if(_objects[k].parameters[p]==searchedCollection[p].Attr)
                            {
                                searchedCollection[p].Count++;
                            }
                        }
                    }
                    if(searchedCollection[p].Count!=0)
                    {
                        probability *= (searchedCollection[p].Count / decisionClass[i].count);
                    }
                    else
                    {
                        //Pomijam tutaj zero, lambde ustawiam na stałe na 1
                        probability *= 1 / (decisionClass[i].count + 1* searchedCollection.Count);
                    }
                    searchedCollection[p].Count = 0;
                }
                classProbability.Add(new KeyVal<string, double>(decisionClass[i].decision,probability));
            }

            var result = classProbability.OrderByDescending(x => x.Count).First();

            return result.Attr;
        }

        private class KeyVal<Key, Val>
        {
            public Key Attr { get; set; }
            public Val Count { get; set; }

            public KeyVal() { }

            public KeyVal(Key key, Val val)
            {
                this.Attr = key;
                this.Count = val;
            }
        }
    }
}

