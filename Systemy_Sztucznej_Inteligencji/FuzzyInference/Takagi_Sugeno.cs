﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuzzyInference
{
    public class Takagi_Sugeno
    {
        private string pathColl { get; set; }

        private DataCollection dataCollection = new DataCollection();

        private List<double> objectToClassifiy = new List<double>();
        public Takagi_Sugeno(string _pathColl)
        {
            this.pathColl = _pathColl;
            SaveDatabase(pathColl);
        }

        public KeyVal<string, double> Solve()
        {
            List<Object> _objects = dataCollection.GetObjects();
            List<double> test = new List<double>();


            for (int i = 0; i < _objects.Count; i++)
            {
                test.Add(CalculateLifeStyle(_objects[i]));
            }

            return new KeyVal<string, double>
            {
                Attr= _objects[test.IndexOf(test.Max())].label,
                Value = test.Max(),
            };
        }

        public double CalculateLifeStyle(Object City)
        {
            List<double> fuzzyRulePollution = new List<double>();
            List<double> fuzzyRuleTemperature = new List<double>();

            //Wykres temperatury dla miasta
            fuzzyRuleTemperature.Add(
                TrapezMember(0, 0, 0.1, 0.4, City.parameters[0])
                );
            fuzzyRuleTemperature.Add(
               TriangleMember(0.3, 0.5, 0.7, City.parameters[0])
                );
            fuzzyRuleTemperature.Add(
                TrapezMember(0.6, 0.9, 1, 1, City.parameters[0])
                );

            //Wykres zanieczyszczeń dla miasta
            fuzzyRulePollution.Add(
                TrapezMember(0, 0, 0.1, 0.3, City.parameters[1])
                );
            fuzzyRulePollution.Add(
               TriangleMember(0.2, 0.5, 0.8, City.parameters[1])
                );
            fuzzyRulePollution.Add(
                TrapezMember(0.7, 0.8, 1, 1, City.parameters[1])
                );

            List<double> results = new List<double>();

            for (int i = 0; i <= 2; i++)
            {
                for (int k = 0; k <= 2; k++)
                {
                    results.Add(min(fuzzyRuleTemperature[i], fuzzyRulePollution[k]));
                }
            }

            List<double> resultOfRules = new List<double>();

            resultOfRules.Add(0.9);
            resultOfRules.Add(0.8);
            resultOfRules.Add(0.5);

            resultOfRules.Add(1);
            resultOfRules.Add(0.7);
            resultOfRules.Add(0.2);

            resultOfRules.Add(0.6);
            resultOfRules.Add(0.3);
            resultOfRules.Add(0.1);

            double decision = 0;
            for (int i = 0; i < resultOfRules.Count; i++)

            {
                decision += results[i] * resultOfRules[i];
            }

            double tmp = 0;
            for (int i = 0; i < results.Count; i++)
            {
                tmp += results[i];
            }

            return decision / tmp;
        }
        private double TriangleMember(double a, double b, double c, double x)
        {
            if ((a < x) && (x <= b)) return (x - a) / (b - a);
            if ((b < x) && (x < c)) return (c - x) / (c - b);
            return 0;
        }

        private double TrapezMember(double a, double b, double c, double d, double x) 
        {
            if (x <= a) return 0;
            if (x > a && x <= b) return (x - a) / (b - a);
            if (x > b && x < c) return 1;
            if (x >= c && x <= d) return (d - x) / (d - c);
            return 0;
        }

        double prod(double a, double b)
        {
            return a* b;
        }
        double min(double a, double b)
        {
            if (a<b) return a;
            else return b;
        }

        private void SaveDatabase(string path)
        {
            string[] lines = File.ReadAllLines(path);

            for (int i = 0; i < lines.Length; i++)
            {
                string[] tmp = lines[i].Split(',');

                dataCollection.AddObject(tmp.Select(s => s.Replace(".", ",")).ToList());
            }
        }



        public class KeyVal<Key, Val>
        {
            public Key Attr { get; set; }
            public Val Value { get; set; }

            public KeyVal() { }

            public KeyVal(Key key, Val val)
            {
                this.Attr = key;
                this.Value = val;
            }
        }
    }
}
