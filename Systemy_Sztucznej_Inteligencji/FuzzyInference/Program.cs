﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static FuzzyInference.Takagi_Sugeno;

namespace FuzzyInference
{
    class Program
    {
        static void Main(string[] args)
        {
            Takagi_Sugeno takagi_Sugeno = new Takagi_Sugeno("data.txt");

            KeyVal<string, double> result =  takagi_Sugeno.Solve();
            Console.WriteLine($"Miasto: {result.Attr} wskaźnik życia: {result.Value}");
            Console.ReadKey();
        }
    }
}
