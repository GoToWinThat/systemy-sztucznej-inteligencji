﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuzzyInference
{
    class DataCollection
    {
        private List<Object> objects = new List<Object>();

        public void AddObject(List<string> data)
        {

            objects.Add(
                new Object
                {
                    label = data.FirstOrDefault(),
                    parameters = data.Skip(1).Select(Double.Parse).ToList()
                });
        }

        public List<Object> GetObjects()
        {
            return objects;
        }
    }

    public class Object
    {
        public string label { get; set; }

        public List<double> parameters = new List<double>();
    }
}
