﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Introduction
{
    public class ImageModifier
    {
        private Bitmap img { get; set; }
        
        public ImageModifier(string _path)
        {
           img= new Bitmap(_path);
        }
        public void ApplyMaskandSave()
        {
            Bitmap newImage = new Bitmap(img.Width, img.Height);
            double[][] filterMatrix = new double[3][];
            filterMatrix[0] = new double[] { 1,1,1 };
            filterMatrix[1] = new double[] { 1,1,1 };
            filterMatrix[2] = new double[] { 1,1,1};

            for (int i = 1; i < img.Width - 1; i++)
            {
                for (int k = 1; k < img.Height - 1; k++)
                {
 
                    Color px = img.GetPixel(i, k);

                    double newR = 0;
                    double newG = 0;
                    double newB = 0;
                    int X = i;
                    int Y = k;

                    for (int a = -1; a < 2; a++)
                    {
                        for (int b = -1; b < 2; b++)
                        {
                            int mask = (int)filterMatrix[a + 1][b + 1];
                            newR += (img.GetPixel(X + a, Y + b).R * mask);
                            newG += (img.GetPixel(X + a, Y + b).G * mask);
                            newB += (img.GetPixel(X + a, Y + b).B * mask);
                        }
                    }
                    newR /= 9;
                    newG/= 9;
                    newB /= 9;
                    newR = CompareRGB(newR);
                    newG = CompareRGB(newG);
                    newB = CompareRGB(newB);
                    newImage.SetPixel(i, k, Color.FromArgb(Convert.ToInt32(newR), Convert.ToInt32(newG), Convert.ToInt32(newB)));
                }
            }
            newImage.Save(@"photo_mask.jpg");

        }
        public void ApplyBasic()
        {
            Bitmap photo = new Bitmap(img.Width, img.Height);
            for (int i = 0; i < photo.Width; i++)
            {
                for (int k = 0; k < photo.Height; k++)
                {
                    Color pxl = img.GetPixel(i, k);
                    int avg = (pxl.R + pxl.G + pxl.B) / 3;
                    photo.SetPixel(i, k, Color.FromArgb(avg, avg, avg));
                }
            }

            photo.Save("photo_basic.jpg");
        }
        private double CompareRGB(double val)
        {
            if (val > 255)
                return 255;
            if (val < 0)
                return 0;
            return val;
        }
    }
}
