﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Introduction
{
    public class IrisDatabase
    {
        private string path { get; set; }
        private double[][] data;
        public IrisDatabase(string _path)
        {
            this.path = _path;
            SaveDatabase(path);
        }

        private void SaveDatabase(string path)
        {
            string[] lines = File.ReadAllLines(path);

            //Save Data
            data = new double[lines.Length][];
            for (int i = 0; i < lines.Length; i++)
            {
                string[] tmp = lines[i].Split(',');
                data[i] = new double[tmp.Length + 2];
                for (int j = 0; j < tmp.Length; j++)
                {
                    if (j >= 4)
                    {
                        if (tmp[j] == "Iris-setosa")
                        {
                            data[i][4] = 0;
                            data[i][5] = 0;
                            data[i][6] = 1;
                        }
                        if (tmp[j] == "Iris-versicolor")
                        {
                            data[i][4] = 0;
                            data[i][5] = 1;
                            data[i][6] = 0;
                        }
                        if (tmp[j] == "Iris-virginica")
                        {
                            data[i][4] = 1;
                            data[i][5] = 0;
                            data[i][6] = 0;
                        }
                    }
                    else
                    {
                        data[i][j] = Convert.ToDouble(tmp[j].Replace(".", ","));
                    }
                }
            }

            //Normalize Data
            for (int i = 0; i < 4; i++)
            {
                SetColumn(
                    ref data,
                    Normalizator.Normalize(GetColumn(data,i), 1.0, 0.0),
                    i
                    );
            }

            Shuffler.Shuffle(data);

        }  

        public void ShowData()
        {
            for(int i=0; i<data.GetLength(0); i++)
            {
                for(int k=0; k<data[i].Length;k++)
                {
                    Console.Write($"{data[i][k]} || ");
                }
                Console.WriteLine();
            }
        }

        private double[] GetColumn(double[][] matrix,int index)
        {
            double[] result = new double[matrix.GetLength(0)];
            for(int i=0;i<matrix.GetLength(0); i++)
            {
                result[i] = matrix[i][index];
            }
            return result;
        }

        private void SetColumn(ref double[][] matrix,double[] column,int index)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                matrix[i][index] = column[i];
            }
        }
    }
}
