﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Introduction
{
    public class Shuffler
    {
        public static void Shuffle<T>(T[] data)
        {
            Random rand = new Random();
            for (int i = 0; i < data.Length; i++)
            {
                Swap<T>(data, i, rand.Next(0, data.Length - 1));
            }
        }

        private static void Swap<T>(T[] data, int a, int b)
        {
            T temp = data[a];
            data[a] = data[b];
            data[b] = temp;
        }
    }
}
