﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Introduction
{
    public class Normalizator
    {
        static double  Max(double[] numbers)
        {
            double result = double.MinValue;
            for (int i = 0; i < numbers.Length; i++)
            {
                if (result < numbers[i])
                    result = numbers[i];
            }
            return result;
        }

        static double Min(double[] numbers)
        {
            double result = double.MaxValue;
            for (int i = 0; i < numbers.Length; i++)
            {
                if (result > numbers[i])
                    result = numbers[i];
            }
            return result;
        }

        public static double[] Normalize(double[] data, double nmin, double nmax)
        {
            double[] result = new double[data.Length];
            double min = Min(data);
            double max = Max(data);

            for (int i = 0; i < data.Length; i++)
            {
                result[i] = (((data[i] - min) / (max - min)) * (nmax - nmin)) + nmin;
            }
            return result;
        }
    }
}
