﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace K_NN
{
    public class K_NN_Classifier
    {
        private string pathColl { get; set; }
        private string pathParam { get; set; }
        private DataCollection dataCollection = new DataCollection();
        private List<double> objectToClassifiy = new List<double>();
        public K_NN_Classifier(string _pathColl, string _pathParam)
        {
            this.pathColl = _pathColl;
            this.pathParam = _pathParam;
            SaveDatabase(pathColl);
            SaveObject();
        }

        public string Solve()
        {
            List<Object> _objects = dataCollection.GetObjects();
            List<KeyVal<string, double>> vector = new List<KeyVal<string, double>>();
            //tworzy wektor/lista  150 dlugosc od punktu zadanego p do tych 150 - baza
            for (int i=0; i< _objects.Count;i++)
            {
                vector.Add(
                    new KeyVal<string, double>
                    {
                        //attr - nazwa kwiatu -> listy kwiatow gdzie label to nawa
                        Attr=_objects[i].label,
                        Value=CalculateLenght(objectToClassifiy,_objects[i].parameters)
                    }
                    );
            }

            var result = vector.OrderBy(x => x.Value).Take((int)(vector.Count() * 0.1)).ToList();

            return FindType(result);
        }
        private void SaveDatabase(string path)
        {
            string[] lines = File.ReadAllLines(path);

            for (int i = 0; i < lines.Length; i++)
            {
                string[] tmp = lines[i].Split(',');

                dataCollection.AddObject(tmp.Select(s=>s.Replace(".", ",")).ToList());
            }    
        }
        private void SaveObject()
        {
            string data = File.ReadAllText(pathParam);
            List<string> tmp = data.Split(',').Select(s => s.Replace(".", ",")).ToList();          
            for (int i = 0; i < tmp.Count; i++)
            {
                objectToClassifiy.Add(Convert.ToDouble(tmp[i]));
            }
        }

        private double CalculateLenght(List<double> x, List<double>y)
        {
            double result = 0;
            if(x.Count!=y.Count)
            {
                throw new InvalidDataException();
            }
            for(int i=0; i<x.Count; i++)
            {
                result += Math.Pow(x[i]-y[i],2);
            }

            return Math.Sqrt(result);
        }

        private string FindType(List<KeyVal<string, double>> data)
        {
            List<string> types = new List<string>();
            List<int> count = new List<int>();
            foreach(var d in data)
            {
                if(!types.Contains(d.Attr))
                {
                    types.Add(d.Attr);
                    count.Add(1);
                }
                else
                {
                    count[types.IndexOf(d.Attr)] += 1;
                }
            }

            return types[count.IndexOf(count.Max())];
        }
        private class KeyVal<Key, Val>
        {
            public Key Attr { get; set; }
            public Val Value { get; set; }

            public KeyVal() { }

            public KeyVal(Key key, Val val)
            {
                this.Attr = key;
                this.Value = val;
            }
        }
    }
}
