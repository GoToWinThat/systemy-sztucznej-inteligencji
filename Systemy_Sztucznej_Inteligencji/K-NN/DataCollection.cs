﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace K_NN
{
    class DataCollection
    {
        // lista kwiatow
        private List<Object> objects = new List<Object>();

        public void AddObject(List<string> data)
        {

            objects.Add(
                new Object
                {
                    label = data.Last(),
                    parameters = DeleteLast(data).Select(Double.Parse).ToList()
                }); ; ;
        }

        public List<Object> GetObjects()
        {
            return objects;
        }

        private List<string> DeleteLast(List<string> list)
        {
            list.RemoveAt(list.Count() - 1);
            return list;
        }
    }

    public class Object
    {
        // nazwa - iris setosa
        public string label { get; set; }

        // lista xyzc

        public List<double> parameters = new List<double>();
    }
}

