﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace K_NN
{
    class Program
    {
        static void Main(string[] args)
        {
            K_NN_Classifier k_nn_Classifier = new K_NN_Classifier("IrisDatabase.txt", "searchingParams.txt");
            
            Console.WriteLine(k_nn_Classifier.Solve());

            Console.ReadKey();
        }
    }
}
